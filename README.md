# MyUW Images

## Purpose

This repository is a place to store images that MyUW uses.  The need comes up
when you want to keep images out of a code repository but don't want to use non-
university hosting service like imgur, flickr, or using git hosting urls.  
This is just a simple file structure.

## How to add content

If there is shared content across the MyUW apps use the shared folder in the
content images directory.  If you need the images for a specific module, create
a new folder with your module name and proceed to use a directory structure that
makes sense for your needs.

If there is shared screenshots images across the MyUW apps, use the shared 
folder in the screnshots images directory.  If you need the images for a 
specific module, create a new folder with your module name and proceed to use a 
directory structure that makes sense for your needs.

## How to use content

Upon a commit to this repository, this repo will be added to the MyUW
environments that use a snapshot of this repository (predev as of now).
All images are located under /images using the
file structure of the repo.  Example - /images/contentImages/careerLocker/
careerLockerLoginButton.gif

## Things for developers to consider

Changing the file structure of this repository will change the 
file structure of the urls.  Proceed with extreme caution.

This repository is only for media files.  Updating binary files often will
result in massive respositories.  Better to use this repository as a repository
of things that do very seldomly change, e.g., logos and screenshots.  Best 
practice if you need two logos for different environments temporarily is to add
the new logo with a different file name ex `updated_logo.png`.  This repository
will not use different branches for different uses cases (example different
branches for different tiers).

Be respectful of our users, users don't want to download large files, and we
don't want to store them either.

## Local Development

You can serve up the content anyway you want for your applications.

### You can deploy to a Running Local Tomcat

We added in support to deploy the artifact to Tomcat using Maven. To setup add a
server to your .m2/settings.xml for Tomcat. Example:

<server>
   <id>TomcatServer</id>
   <username>user</username>
   <password>password</password>
</server>
The id of TomcatServer is important here. Add that user/pass combo to your 
$TOMCAT_HOME/conf/tomcat-users.xml. Also be sure you have a role of manager 
listed.

Example:

<role rolename="manager"/>
<user username="user" password="password" roles="manager-script"/>
The role of manager-script gives them the ability to use the /text api from 
Tomcat.

Read more about that here: 
http://tomcat.apache.org/maven-plugin-2.0/tomcat7-maven-plugin/plugin-info.html

With this you can run mvn tomcat7:deploy or mvn tomcat7:redeploy if you have 
already deployed it once. We also wrote a script for this. Just run ./build.sh